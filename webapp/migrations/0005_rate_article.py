# Generated by Django 2.1 on 2018-12-18 09:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0004_auto_20181218_0958'),
    ]

    operations = [
        migrations.AddField(
            model_name='rate',
            name='article',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='article', to='webapp.Article', verbose_name='Статья'),
            preserve_default=False,
        ),
    ]
