from django import forms
from webapp.models import *


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'text', 'author']


class CommentFormCreate(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['user', 'parent_id', 'text']


class CommentFormUpdate(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['parent_id', 'text']

