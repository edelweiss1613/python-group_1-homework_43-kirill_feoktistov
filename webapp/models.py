from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    text = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Текст')
    author = models.ForeignKey(User, on_delete=models.PROTECT, related_name='articles', verbose_name='Автор')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')

    def __str__(self):
        return self.title


class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='info', verbose_name='Пользователь')
    favorites = models.ManyToManyField(Article, related_name='favorites', verbose_name='Избранное')

    def __str__(self):
        return self.user.username


class Rate(models.Model):
    RATE_HORRIBLE = 'horrible'
    RATE_BAD = 'bad'
    RATE_NORMAL = 'normal'
    RATE_GOOD = 'good'
    RATE_BEST = 'best'

    RATE_CHOICES = (
        (RATE_HORRIBLE, 'Ужасно'),
        (RATE_BAD, 'Плохо'),
        (RATE_NORMAL, 'Нормально'),
        (RATE_GOOD, 'Хорошо'),
        (RATE_BEST, 'Отлично'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='rates', verbose_name='Пользователь')
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='rates', verbose_name='Статья')
    rate = models.CharField(max_length=20, choices=RATE_CHOICES, default=RATE_BEST, verbose_name='Статус')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')

    class Meta:
        unique_together = ('user', 'article',)

    def __str__(self):
        return "%s / %s / %s" % (self.user, self.article, self.get_rate_display())


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments', verbose_name='Пользователь')
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments', verbose_name='Статья')
    parent_id = models.ForeignKey('Comment', null=True, blank=True, related_name='children', on_delete=models.CASCADE, verbose_name='Родительский коммент')
    text = models.TextField(max_length=3000, null=False, blank=False, verbose_name='Текст комментария')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')

    def __str__(self):
        return "(%s) %s | %s" % (self.pk, self.user, self.article)
