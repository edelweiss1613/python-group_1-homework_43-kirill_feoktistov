from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from webapp.forms import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
# Create your views here.


class ArticleListView(ListView):
    model = Article
    template_name = 'index.html'


class ArticleCreateView(CreateView):
    model = Article
    form_class = ArticleForm
    template_name = 'article_create.html'
    success_url = reverse_lazy('index')


class ArticleUpdateView(UpdateView):
    model = Article
    form_class = ArticleForm
    template_name = 'article_update.html'
    success_url = reverse_lazy('index')


class ArticleDeleteView(DeleteView):
    model = Article
    template_name = 'article_delete.html'
    success_url = reverse_lazy('index')


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'article_detail.html'


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentFormCreate
    template_name = 'comment_create.html'
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        form.instance.article = get_object_or_404(Article, pk=self.kwargs['pk'])
        form.save()
        return super(CommentCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['apk'] = self.kwargs['pk']
        return context


class CommentUpdateView(UpdateView):
    model = Comment
    form_class = CommentFormUpdate
    template_name = 'comment_update.html'
    success_url = reverse_lazy('index')


class UserListView(ListView):
    model = User
    template_name = 'user_list.html'


class UserDetailView(DetailView):
    model = User
    template_name = 'user_detail.html'


class SearchView(ListView):
    model = Article
    template_name = 'search.html'

    def get_queryset(self):
        article_title = self.request.GET.get('article_title')
        self.query_text = article_title
        self.article_lists = Article.objects.filter(title__icontains=article_title) | Article.objects.filter(text__icontains=article_title)
        return self.article_lists

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['article_lists'] = self.article_lists
        context['query_text'] = self.query_text
        return context


