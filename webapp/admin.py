from django.contrib import admin
from webapp.models import *

# Register your models here.

class CommentAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'user', 'article', 'parent_id')

admin.site.register(UserInfo)
admin.site.register(Article)
admin.site.register(Rate)
admin.site.register(Comment, CommentAdmin)